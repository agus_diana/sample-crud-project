import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import { BootstrapVue, IconsPlugin, BootstrapVueIcons, FormPlugin, LayoutPlugin, ModalPlugin, CardPlugin, DropdownPlugin, TablePlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue, BootstrapVueIcons, FormPlugin, LayoutPlugin, ModalPlugin, CardPlugin, DropdownPlugin, TablePlugin)
Vue.use(IconsPlugin)
Vue.config.productionTip = false

Vue.prototype.$http = axios
Vue.prototype.$http.defaults.baseURL = 'http://localhost:8111'
Vue.prototype.$http.defaults.headers = { Accept: 'application/json' }
Vue.prototype.$http.defaults.timeout = 10000
Vue.prototype.$http.interceptors.response.use(function (response) {
  return response.data
}, function (error) {
  // Do something with response error
  console.log(error)
  return Promise.reject(error)
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
