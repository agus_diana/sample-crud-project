<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'customers'], function ($customers){
    $customers->get('',['uses' => 'CustomerController@index']);
    $customers->post('',['uses' => 'CustomerController@store']);
    $customers->put('{id}',['uses' => 'CustomerController@update']);
    $customers->get('{id}',['uses' => 'CustomerController@show']);
    $customers->delete('{id}',['uses' => 'CustomerController@destroy']);
});
