<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 27/07/2021
 * Time: 10.45
 */

namespace App\Http\Controllers;

use App\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    public function index(Request $request){
        $order = ($request->input('order') ? $request->input('order') : 'created_at');
        $order_by = ($request->input('order_by') ? $request->input('order_by') : 'desc');
        $size = ($request->input('size') ? $request->input('size') : 10);
        $keywords = $request->input('keywords');

        $customersQuery = Customers::query();
        $customersQuery->orderBy($order,$order_by);
        if ($keywords)  {
            $customersQuery->orWhere('name','like','%' . $keywords . '%');
            $customersQuery->orWhere('email','like','%' . $keywords . '%');
        }
        $customers = $customersQuery->Paginate($size);

        return $this->response($customers);
    }

    public function show($id){
        $customer = Customers::find($id);
        if (!$customer) return $this->response(null,400,'error','customer not found');
        return $this->response($customer);
    }

    public function store(Request $request){
        $requestForm = $request->all();

        $validation = Validator::make($requestForm, [
            'name' => 'required',
            'email' => 'required|email|unique:customers,email',
            'password' => 'required',
            'gender' => 'required|in:m,f',
            'address' => 'required',
        ]);

        if($validation->fails()) return $this->response($validation->errors(),203,'error','error validation');

        $customer = Customers::create([
            'name' => $requestForm['name'],
            'email' => $requestForm['email'],
            'password' => password_hash($requestForm['password'], PASSWORD_BCRYPT, ['cost' => 12]),
            'gender' => $requestForm['gender'],
            'is_married' => $request->input('is_married') == true ? $requestForm['is_married'] : 0,
            'address' => $requestForm['address'],
        ]);
        return $this->response($customer,201);
    }

    public function update(Request $request, $id){
        $requestForm = $request->all();

        $customer = Customers::find($id);
        if (!$customer) return $this->response(null,400,'error','customer not found');

        $validation = Validator::make($requestForm, [
            'name' => 'required',
            'gender' => 'required|in:m,f',
            'address' => 'required',
        ]);

        if($validation->fails()) return $this->response($validation->errors(),203,'error','error validation');

        $customer->name = $requestForm['name'];
        if ($request->input('password')) $customer->password = $requestForm['password'];
        $customer->gender = $requestForm['gender'];
        $customer->is_married = $request->input('is_married') == true ? $requestForm['is_married'] : 0;
        $customer->address = $requestForm['address'];
        $customer->save();

        $customerData = $customer->refresh();
        return $this->response($customerData,200);
    }

    public function destroy($id){
        $customer = Customers::find($id);
        if (!$customer) return $this->response(null,400,'error','customer not found');
        $customer->delete();
        return $this->response(null,200,'ok','customer deleted');
    }
}
