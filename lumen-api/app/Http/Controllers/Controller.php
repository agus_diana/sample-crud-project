<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function response($data, $code = 200, $message = 'success', $responseMsg = 'ok')
    {
        return response()->json([
            'status' => [
                'code' => $code,
                'response' => $message,
                'message' => $responseMsg
            ],
            'result' => $data
        ], $code);
    }
}
