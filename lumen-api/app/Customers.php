<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 27/07/2021
 * Time: 10.39
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
class Customers extends Model
{
    protected $fillable = [
        'name','email','password','gender','is_married','address'
    ];

    protected $hidden = ['password'];
}
