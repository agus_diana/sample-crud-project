# Sample API CRUD

A starter template to develop API with Lumen 7.

## requirements

- Minimum php 7.2 (tested)
- Database MySQL / MariaDB (tested)

## Installation

- `git clone`
- `cd lumen-api`
- `composer install`

## Configuration

- `copy .env.example .env`
- Edit `.env` file ford database connection configuration.
- `php artisan migrate`
- `php artisan db:seed`
- `php -S localhost:8111 -t public`

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
