<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;
class customersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i = 1; $i <= 100; $i++){

            // insert data ke table pegawai menggunakan Faker
            DB::table('customers')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => Hash::make('password'),
                'gender' => $faker->randomElement(['m','f']),
                'is_married' => $faker->numberBetween(0,1),
                'address' => $faker->address,
            ]);

        }
    }
}
