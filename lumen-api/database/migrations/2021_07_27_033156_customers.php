<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Customers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function(Blueprint $table){
            $table->id();
            $table->string('name',125);
            $table->string('email',125);
            $table->string('password');
            $table->string('gender',3)->comment('f is female, m is male');
            $table->smallInteger('is_married')->comment('0 is unmarried, 1 is married');
            $table->string('address',225);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }
}
